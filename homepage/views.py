from django.shortcuts import render, get_object_or_404, redirect
from django.conf import settings
from django.core.mail import send_mail
from .forms import ScheduleForm, BlogForm

from .models import ScheduleModel, BlogModel, Category
from django.contrib import messages

from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect


class BlogView(ListView):
    model = BlogModel
    template_name = '3_blog.html'
    ordering = ['-post_date']
    cat_menu = Category.objects.all()
 

    # def get_context_data(self,*args,**kwargs):
    #     cat_menu = Category.objects.all()
    #     print(cat_menu)
    #     context = super(BlogView,self).get_context_data(*args,**kwargs)
    #     context['cat_menu'] = cat_menu
    #     return context

class PostView(DetailView):
    model = BlogModel
    template_name = '3b_blog-post.html'

    def get_context_data(self,*args, **kwargs):
        context = super(PostView,self).get_context_data(*args, **kwargs)
        stuff = get_object_or_404(BlogModel, id = self.kwargs['pk'])
        total_likes = stuff.total_likes()

        liked = False
        if stuff.likes.filter(id = self.request.user.id ).exists():
            liked = True

        context['total_likes'] = total_likes
        context['liked'] = liked
        return context

class AddPostView(CreateView):
    model = BlogModel
    form_class = BlogForm
    template_name = '3c_add_post.html'

    # fields = '__all__'

class UpdatePostView(UpdateView):
    model = BlogModel
    form_class = BlogForm
    template_name = '3d_update_post.html'
    # fields = ['title','title_tag','body']

class DeletePostView(DeleteView):
    model = BlogModel
    template_name = '3e_delete.html'
    success_url = reverse_lazy('homepage:blog')
    
class AddCategoryView(CreateView):
    model = Category
    template_name = '3f_add_cat.html'
    fields = '__all__'
  

def category_page(request, category):
    cat = category.replace('-',' ')
    data = BlogModel.objects.filter(category = cat)
    return render(request,'3g_category_page.html', {'title':cat, 'data':data})

def LikeView(request,pk):
    post = get_object_or_404(BlogModel, id= request.POST.get('post_id'))
    liked = False
    if post.likes.filter(id=request.user.id).exists():
        post.likes.remove(request.user)
        liked = False
    else:
        post.likes.add(request.user)
        liked = True
    return HttpResponseRedirect(reverse('homepage:detail_post', args=[str(pk)]))

# Function Based View
def blog(request):
    BlogModel.objects.all().remove()
    if request.method == 'GET':
        posts = BlogModel.objects.all()
        print(posts)
        print("AA")
        return render(request, '3_blog.html', {'posts':posts})
   
def post(request,pk):
    posting = get_object_or_404(BlogModel, pk=pk)
    return render(request, '3b_blog-post.html', {'posting':posting})

def index(request):
    return render(request, '1_home.html' )

def skills(request):
    return render(request, '2_skills.html' )

def blog(request):
    return render(request, '3_blog.html' )

def contact(request):
    if request.method == 'POST':
        mail = request.POST['mail']
        title = request.POST['title']
        message = request.POST['message']
        whole_message = "Email:"+  mail + "\ntitle: " + title + "\nmessage:" + message
        send_mail('['+mail+ ' sent you a message]',
        whole_message,
        settings.EMAIL_HOST_USER,
        ['fathinahai@gmail.com'],
        fail_silently = False)
        context = {'display':'<h3>Your message is sent! I will reach out to you shortly</h3>'}
        return render(request, '4_contact.html',context)
    return render(request, '4_contact.html')


def showform(request):
    data = ScheduleModel.objects.all()
    return render(request, '6_showform.html', {'data': data})

def delete_activity(request, pk):
    activity = get_object_or_404(ScheduleModel, pk=pk)
    activity.delete()

    return redirect('homepage:showform')

def form(request):
    if request.method == "POST":
        data = ScheduleForm(request.POST)
        if data.is_valid():
            data.save()
            return redirect('homepage:showform')
        else:
            messages.warning(request, 'Data input is not valid, Please try again!')
    else:
        data = ScheduleForm()

    return render(request, '5_form.html', {'form': data})
    


    