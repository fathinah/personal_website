from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import datetime,date

# Create your models here.

class ScheduleModel(models.Model):
    date = models.DateField()
    time = models.TimeField(auto_now_add=False)
    name = models.CharField(max_length=100)
    category = models.CharField(max_length=50)

    def __str__(self):
        return "{}.{}".format(self.id, self.name)

class CategoryManager(models.Manager):
    def create_cate(self, name):
        cat = self.create(name=name)
        # do something with the book
        return cat

class Category(models.Model):
    name = models.CharField(max_length=250)
    objects = CategoryManager()
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('homepage:blog')
    
# cat1 = Category.objects.create_cate("HIIHIH Science")
# cat1.save()
# print("hi")
# print(Category.objects.all().count())

class BlogModel(models.Model):
    title = models.CharField(max_length=250, default="Title")
    title_tag = models.CharField(max_length=100)
    category = models.CharField(max_length=250)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(null=True, blank=True, upload_to="images/")
    body = models.TextField(blank=True, null=True)
    post_date = models.DateField(auto_now_add=True)
    snippet = models.CharField(max_length=150)
    likes = models.ManyToManyField(User, related_name = 'blog_posts')
    
    def total_likes(self):
        return self.likes.count()
    def __str__(self):
        return self.title+ " | " + str(self.author)

    def get_absolute_url(self):
        return reverse('homepage:detail_post', kwargs={'pk': self.pk})

