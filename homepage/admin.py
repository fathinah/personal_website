from django.contrib import admin
from .models import BlogModel, ScheduleModel, Category
# Register your models here.

admin.site.register(BlogModel)
admin.site.register(ScheduleModel)
admin.site.register(Category)