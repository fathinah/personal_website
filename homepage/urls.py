from django.urls import path
from . import views

app_name = 'homepage'


urlpatterns = [
    path('blog/', views.BlogView.as_view() , name='blog'),
    path('post/<int:pk>/', views.PostView.as_view(), name='detail_post'),
    path('add_post/', views.AddPostView.as_view(), name='add_post'),
    path('post/edit/<int:pk>/', views.UpdatePostView.as_view(), name='edit_post'),
    path('post/delete/<int:pk>/', views.DeletePostView.as_view(), name='delete_post'),
    path('add_category/', views.AddCategoryView.as_view(), name='add_category'),
    path('category/<str:category>', views.category_page, name='category_page'),
    path('like/<int:pk>', views.LikeView, name='like_post'),

    
    path('', views.index, name='index'),
    path('skills/', views.skills, name='skills'),
    path('blog/', views.blog, name='blog'),
    path('contact/', views.contact, name='contact'),
    path('form/', views.form, name='form'),
    path('show-form/', views.showform, name='showform'),
    path('delete-activity/<int:pk>/', views.delete_activity, name='delete_activity'),
   

]