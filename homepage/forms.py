from django import forms
from .models import ScheduleModel, BlogModel, Category
import datetime

cats = Category.objects.all().values_list('name','name')

class ScheduleForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Project's Title",}))
    category = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Project's Category",}))
    date = forms.DateField(input_formats=['%Y-%m-%d'], initial=datetime.date.today(), widget=forms.TextInput(attrs={'class': 'form-control'}))
    time = forms.TimeField(input_formats=['%H:%M:%S'], initial=datetime.time, widget=forms.TextInput(attrs={'class': 'form-control'}))
    class Meta:
        model = ScheduleModel
        fields = ['date','time','name','category',]


class BlogForm(forms.ModelForm):
    class Meta:
        model= BlogModel
        fields = ('title','title_tag','category','author','image','body','snippet')
        widgets = {
            'title': forms.TextInput(attrs={'class=':'form-control'}),
            'title_tag': forms.TextInput(attrs={'class=':'form-control'}),
            'category': forms.Select(choices=cats,attrs={'class=':'form-control'}),
            'author': forms.Select(attrs={'class=':'form-control'}),
            'body': forms.TextInput(attrs={'class=':'form-control'}),
            'snippet': forms.Textarea(attrs={'class=':'form-control'}),
        }
