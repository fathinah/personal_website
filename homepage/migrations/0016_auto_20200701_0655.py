# Generated by Django 3.0.7 on 2020-07-01 05:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0015_auto_20200629_1822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogmodel',
            name='body',
            field=models.TextField(blank=True, null=True),
        ),
    ]
