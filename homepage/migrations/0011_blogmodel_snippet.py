# Generated by Django 3.0.7 on 2020-06-29 16:21

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0010_auto_20200629_1549'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogmodel',
            name='snippet',
            field=models.CharField(default=django.utils.timezone.now, max_length=100),
            preserve_default=False,
        ),
    ]
