# Generated by Django 3.0.7 on 2020-06-26 11:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0005_blogmodel_post_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='Contemplation', max_length=250)),
            ],
        ),
        migrations.AddField(
            model_name='blogmodel',
            name='category',
            field=models.CharField(default='Exchange', max_length=250),
        ),
    ]
