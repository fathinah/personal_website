# Generated by Django 2.2.5 on 2020-06-27 15:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0007_auto_20200626_1321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogmodel',
            name='category',
            field=models.CharField(max_length=250),
        ),
    ]
