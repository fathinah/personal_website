from django import template

register = template.Library()

from ..models import Category

# @register.simple_tag()
# def get_categories():
#     return Category.objects.all()
@register.simple_tag
def test_tag():
    return Category.objects.all()

@register.inclusion_tag('all_categories.html')
def get_categories():
    categories = Category.objects.all()
    return{
        'categories': categories
    }
